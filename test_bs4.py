#import downloadRE
import requests

from bs4 import BeautifulSoup
from pprint import pprint


def download(url, num_retries=2, user_agent='Chrome/42.0.2311.135', charset='utf-8', proxies=None): #for creating heml string of url
    #print('Downloading:', url)
    headers = {'User-agent': user_agent}
    try:
        resp = requests.get(url, headers=headers, proxies=proxies)
        html = resp.text
        if resp.status_code > 400:
            print('Download error ->', resp.text)
            html = None
            if num_retries and 500 <= resp.status_code < 600:
                return download(url, num_retries-1)
    except requests.exceptions.RequestException as e:
        print('Download error ->', e)
        html = None
    return html

def news_pull(url):
    html = download(url)
    soup = BeautifulSoup(html, 'html5lib')
    li = soup.findAll('li', class_='mm')
    news_set = []

    for data in li:
        link = parse(url) + data.a['href']
        news_data = news_text(link)
        author = news_data[1]
        text = news_data[0]
        news_set.append({'title':data.a.text, 'link': link, 'date':data.span.a.text, 'text':text, 'author':author})
    
    return news_set

def news_text(url):
    html = download(url)
    soup = BeautifulSoup(html, 'html5lib')
    td_main = soup.find('td', class_='eText')
    try:
        text = td_main.span.next_element.text
    except:
        text = None
    td_detail = soup.findAll('td', class_='eDetails1')[1]
    author = td_detail.findAll('a')[1].text

    return [text, author]

def parse(url):
    if url[len(url)-1] == '/':
        parse_url = url[:len(url)-1]
    else:
        parse_url = url

    parse_url = parse_url[:parse_url.rfind('/')]

    return parse_url        

pprint(news_pull('http://izhcommunal.ru/news/'))