import urllib.request
import re
import itertools
import requests

from urllib.error import URLError, HTTPError, ContentTooShortError
from urllib.parse import urljoin
from urllib import robotparser



def download(url, num_retries=2, user_agent='Chrome/42.0.2311.135', charset='utf-8', proxies=None): #for creating heml string of url
    #print('Downloading:', url)
    headers = {'User-agent': user_agent}
    try:
        resp = requests.get(url, headers=headers, proxies=proxies)
        html = resp.text
        if resp.status_code > 400:
            print('Download error ->', resp.text)
            html = None
            if num_retries and 500 <= resp.status_code < 600:
                return download(url, num_retries-1)
    except requests.exceptions.RequestException as e:
        print('Download error ->', e)
        html = None
    return html

def get_robot_parser(robots_url):
    """Return the robot parser object using the robots_url"""
    rp = robotparser.RobotFileParser()
    rp.set_url(robots_url)
    rp.read()
    return rp

def crawl_sitemap(url): #for find all links in sitemap (xml) 
    # download the sitemap file
    sitemap = download(url)
    # extract the sitemap links
    print(sitemap)
    links = re.findall('<loc>(.*?)</loc>', sitemap) #find all <loc> ... </loc> string in sitemap
    # download each link
    with open('sitemap.txt', 'w') as myfile:
        for link in links:
            html = download(link) #print all links in url
            myfile.write(link+'\n')
            # scrape html here

def crawl_site(url, max_errors=5): # for geting site links ennumiration method
    for page in itertools.count(1): #endless progression 
        pg_url = '{}{}'.format(url, page) # create new url sting with progression number
        html = download(pg_url) #take html string from pg_url
        if html is None: #retry connection
            num_errors += 1
            print('ошибок:', num_errors)
            if num_errors == max_errors:
                break
        else:
            num_errors = 0

def link_crawler(start_url, link_regex, robots_url=None, user_agent='Chrome/42.0.2311.135'): #find in link all site by template
    """ Crawl from the given start URL following links matched by
    link_regex
    """
    crawl_queue = [start_url] #create new list
    
    if not robots_url:
        robots_url = '{}/robots.txt'.format(start_url)
    
    rp = get_robot_parser(robots_url)
    with open('some.txt', 'w') as myfile:
        seen = set(crawl_queue)
        while crawl_queue:
            url = crawl_queue.pop() #reading last link and delete from list
            try:
                if rp.can_fetch(user_agent, url):
                    #print(url)
                    html = download(url) #get html string from link
                else:
                    print('Blocked by robots.txt:', url)
                    continue
            except (UnicodeDecodeError) as e:
                print('Decode error')
                continue
            if not html: # if html is exist continue (???? for what)
                continue
            for link in get_links(html): # return all links from html string
                if re.match(link_regex, link): # if geting link satisfying template adding into main list 
                    abs_link = urljoin(start_url, link)
                    if abs_link not in seen:
                        seen.add(abs_link)
                        myfile.write(abs_link + '\n')
                        crawl_queue.append(abs_link)

def get_links(html): #return all links in html string
    """
    Return a list of links from html
    """
    #print(html)
    webpage_regex = re.compile("""<a[^>]+href=["'](.*?)["']""", re.IGNORECASE) #regular find '<a' if next sybol not '>' find 'href=' next ' or " next some text and ' or "
    #print(webpage_regex.findall(html))
    return webpage_regex.findall(html) #return all  string satisfying the conditions 
            