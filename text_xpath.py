import time
import requests
from lxml import etree
from typing import Text
from pprint import pprint
from lxml.html import fromstring



def download(url, num_retries=2, user_agent='Chrome/42.0.2311.135', charset='utf-8', proxies=None): #for creating heml string of url
    #print('Downloading:', url)
    headers = {'User-agent': user_agent}
    try:
        resp = requests.get(url, headers=headers, proxies=proxies)
        html = resp.text
        if resp.status_code > 400:
            print('Download error ->', resp.text)
            html = None
            if num_retries and 500 <= resp.status_code < 600:
                return download(url, num_retries-1)
    except requests.exceptions.RequestException as e:
        print('Download error ->', e)
        html = None
    return html

def news_pull(url):
    html = download(url)
    tree = fromstring(html)
    li = tree.xpath('//li[@class="mm"]')
    news_set = []

    for data in li:
        #pprint(data.xpath('.//a/@href'))
        link = parse(url) + data.xpath('.//a/@href')[0]
        news_data = news_text(link)
        author = news_data[1]
        text = news_data[0]
        news_set.append({'title': data.xpath('.//a/b/text()')[0], 'link': link, 'data': data.xpath('.//a/text()')[0], 'author':author, 'text':text})
    
    return news_set

def xprint(xpath_element):
    return etree.tosting(xpath_element, decode = 'unicode')

def news_text(url):
    html = download(url)
    tree = fromstring(html)
    td_main = tree.xpath('//td[@class="eText"]')
    try:
        text = td_main[0].xpath('.//span/*/text()')
    except:
        text = None

    td_detail = tree.xpath('//td[@class="eDetails1"]')[1]
    author = td_detail.xpath('.//a/text()')[1]
    
    return [text, author]


###def lxml_xpath_scraper(html):
###    """ Using lxml and xpath to extract data from country pages. """
###    tree = fromstring(html)
###    results = {}
###    for field in FIELDS:
###        results[field] = tree.xpath(
###            '//tr[@id="places_%s__row"]/td[@class="w2p_fw"]' % field)[0].text_content()
###    return results

def parse(url):
    if url[len(url)-1] == '/':
        parse_url = url[:len(url)-1]
    else:
        parse_url = url

    parse_url = parse_url[:parse_url.rfind('/')]

    return parse_url


start = time.time()
pprint(news_pull('http://izhcommunal.ru/news/'))
end = time.time()
print('timeis -->', end - start)